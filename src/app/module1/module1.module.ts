import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Module1RoutingModule } from './module1-routing.module';
import { Module1Component } from './module1/module1.component';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";
import {PocTranslateHttpLoader} from "../poc-translate-http-loader";

export function createTranslateLoader(http: HttpClient) {
  return new PocTranslateHttpLoader('http://localhost:4205/assets/i18n/','module1',http );
}

@NgModule({
  declarations: [
    Module1Component
  ],
  exports: [
    Module1Component
  ],
  imports: [
    CommonModule,
    Module1RoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
    })
  ]
})
export class Module1Module { }
