import { Component, OnInit } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-module1',
  templateUrl: './module1.component.html',
  styleUrls: ['./module1.component.css']
})
export class Module1Component implements OnInit {
  ngOnInit(): void {
  }

  constructor(public translate: TranslateService) {
    translate.setDefaultLang('en');
    }

}
