import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {Module1Component} from "./module1/module1.component";

const routes: Routes = [
  {
    path: 'comp1',
    component: Module1Component,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Module1RoutingModule { }
