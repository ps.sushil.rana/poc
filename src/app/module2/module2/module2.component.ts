import { Component, OnInit } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-module2',
  templateUrl: './module2.component.html',
  styleUrls: ['./module2.component.css']
})
export class Module2Component implements OnInit {

  ngOnInit(): void {
  }
  constructor(public translate: TranslateService) {
    translate.setDefaultLang('en');
  }

}
