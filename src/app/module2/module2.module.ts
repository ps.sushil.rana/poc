import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {Module2RoutingModule} from './module2-routing.module';
import {Module2Component} from './module2/module2.component';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {HttpClient} from "@angular/common/http";
import {PocTranslateHttpLoader} from "../poc-translate-http-loader";

export function createTranslateLoader(http: HttpClient) {
  return new PocTranslateHttpLoader('http://localhost:4205/assets/i18n/', 'module2', http);
}

@NgModule({
  declarations: [
    Module2Component
  ],
  exports: [
    Module2Component
  ],
  imports: [
    CommonModule,
    Module2RoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient],
      },
    })
  ]
})
export class Module2Module {
}
