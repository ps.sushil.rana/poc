import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']

})
export class AppComponent {
  user={
    firstName:'ps',
    city:'corpay'
  };

  param = {value: 'world'};
  constructor(public translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.setTranslation('en', {
      'HELLO': 'hello {{value}}'
    });
  }

  getTranslations() {
    this.translate.get('HELLO', {value: 'everyone'}).subscribe((res: string) => {
      console.log(res);
      //=> 'hello world'
    });
  }

}
